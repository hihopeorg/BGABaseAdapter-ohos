/**
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.bingoogolapple.baseadapter;

import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.ListContainer;
import ohos.agp.render.Canvas;
import ohos.agp.utils.Rect;

import java.util.Objects;

public abstract class Adapter<VH extends Adapter.ViewHolder> extends BaseItemProvider {

    public static class ViewHolder {
        public Component itemView;
        public int mItemViewType;
        public int mPosition;

        public ViewHolder(Component itemView) {
            this.itemView = itemView;
        }

        public final int getPosition() {
            return mPosition;
        }
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public final Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        VH viewHolder;
        if (component == null) {
            viewHolder = createViewHolder(componentContainer, getItemComponentType(position));
            component = viewHolder.itemView;
            component.setTag(viewHolder);
        } else {
            viewHolder = (VH) component.getTag();
        }
        bindViewHolder(viewHolder, position);
        component.setLayoutConfig(component.getLayoutConfig());
        return component;
    }

    //////////////////////

    public Adapter() {
    }

    public abstract VH onCreateViewHolder(ComponentContainer var1, int var2);

    public abstract void onBindViewHolder(VH var1, int var2);

    public final VH createViewHolder(ComponentContainer parent, int viewType) {
        VH holder = this.onCreateViewHolder(parent, viewType);
        if (holder.itemView.getComponentParent() != null) {
            throw new IllegalStateException("ViewHolder views must not be attached when created. Ensure that you are "
                    + "not passing 'true' to the attachToRoot parameter of LayoutInflater.inflate(..., boolean " +
                    "attachToRoot)");
        }

        holder.mItemViewType = viewType;
        return holder;
    }

    public final void bindViewHolder(VH holder, int position) {
        holder.mPosition = position;
        this.onBindViewHolder(holder, position);
    }

    @Override
    public int getItemComponentType(int position) {
        return 0;
    }

    @Override
    public long getItemId(int i) {
        return Component.ID_DEFAULT;
    }
}
