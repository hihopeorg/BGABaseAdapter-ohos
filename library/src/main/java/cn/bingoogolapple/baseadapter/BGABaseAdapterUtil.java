/**
 * Copyright 2016 bingoogolapple
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.bingoogolapple.baseadapter;

import ohos.aafwk.ability.AbilityPackage;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.render.Texture;
import ohos.agp.utils.Matrix;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;

import java.util.List;

/**
 * 作者:王浩 邮件:bingoogolapple@gmail.com
 * 创建时间:17/1/6 上午4:04
 * 描述:
 */
public class BGABaseAdapterUtil {
    private static final AbilityPackage sApp;

    static {
        AbilityPackage app = new AbilityPackage();//null;
//        try {
//            app = (AbilityPackage) Class.forName("ohos.app.AppGlobals").getMethod("getInitialApplication").invoke(null);
//            if (app == null)
//                throw new IllegalStateException("Static initialization of Applications must be on main thread.");
//        } catch (final Exception e) {
//            try {
//                app = (AbilityPackage) Class.forName("ohos.app.ActivityThread").getMethod("currentApplication").invoke(null);
//            } catch (final Exception ex) {
//            }
//        } finally {
//            sApp = app;
//        }
        sApp = app;
    }

    private BGABaseAdapterUtil() {
    }

    public static AbilityPackage getApp() {
        return sApp;
    }

    public static int dp2px(float dpValue) {
        return AttrHelper.vp2px(dpValue, getApp());
    }

    public static int sp2px(float dpValue) {
        return AttrHelper.fp2px(dpValue, getApp());
    }

    public static int getDimensionPixelOffset(int resId) {
        return 0;//getApp().getResources().getDimensionPixelOffset(resId);
    }

    public static int getColor(int resId) {
        return getApp().getColor(resId);
    }

    public static Element rotateBitmap(PixelMap inputBitmap) {
        Matrix matrix = new Matrix();
        matrix.setRotate(90, (float) inputBitmap.getImageInfo().size.width / 2, (float) inputBitmap.getImageInfo().size.height / 2);

        float outputX = inputBitmap.getImageInfo().size.height;
        float outputY = 0;

        float x1 = matrix.getTranslateX();
        float y1 = matrix.getTranslateY();
        matrix.postTranslate(outputX - x1, outputY - y1);
        PixelMap.InitializationOptions options = new PixelMap.InitializationOptions();
        options.size = new Size(inputBitmap.getImageInfo().size.height, inputBitmap.getImageInfo().size.width);
        options.pixelFormat = PixelFormat.ARGB_8888;
        PixelMap outputBitmap = PixelMap.create(inputBitmap, options);
        Paint paint = new Paint();
        Canvas canvas = new Canvas(new Texture(outputBitmap));
        canvas.drawPixelMapHolder(new PixelMapHolder(inputBitmap), matrix.getTranslateX(), matrix.getTranslateY(), paint);
        return new PixelMapElement(outputBitmap);
    }

    public static boolean isListNotEmpty(List list) {
        return list != null && !list.isEmpty();
    }
}