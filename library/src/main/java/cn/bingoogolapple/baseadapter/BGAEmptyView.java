/**
 * Copyright 2016 bingoogolapple
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.bingoogolapple.baseadapter;

import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.app.Context;

/**
 * 作者:王浩 邮件:bingoogolapple@gmail.com
 * 创建时间:17/1/9 下午9:45
 * 描述:
 */
public class BGAEmptyView extends StackLayout {
    private Component mContentView;
    private Component mEmptyView;
    private Text mMsgTv;
    private Image mIconIv;
    private Delegate mDelegate;

    public BGAEmptyView(Context context, AttrSet attrs) {
        super(context, attrs);

        if (getChildCount() < 0 || getChildCount() > 2) {
            throw new IllegalStateException(BGAEmptyView.class.getSimpleName() + "只能有一个或两个子控件");
        }
        initView();
        setListener();

        showContentView();
    }

    private void initView() {
        if (getChildCount() == 1) {
            mContentView = getComponentAt(0);
            //View.inflate(getContext(), R.layout.bga_baseadapter_empty_view, this);
            LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_bga_baseadapter_empty_view, this, true);
            mEmptyView = getViewById(ResourceTable.Id_ll_bga_adapter_empty_view_root);
        } else {
            mEmptyView = getComponentAt(0);
            mContentView = getComponentAt(1);
        }

        mMsgTv = getViewById(ResourceTable.Id_tv_bga_adapter_empty_view_msg);
        mIconIv = getViewById(ResourceTable.Id_iv_bga_adapter_empty_view_icon);
    }

    private void setListener() {
        mEmptyView.setClickedListener(new BGAOnNoDoubleClickListener() {
            @Override
            public void onNoDoubleClick(Component v) {
                if (mDelegate != null) {
                    mDelegate.onClickEmptyView(BGAEmptyView.this);
                }
            }
        });
        mIconIv.setClickedListener(new BGAOnNoDoubleClickListener() {
            @Override
            public void onNoDoubleClick(Component v) {
                if (mDelegate != null) {
                    mDelegate.onClickIconEmptyView(BGAEmptyView.this);
                }
            }
        });
        mMsgTv.setClickedListener(new BGAOnNoDoubleClickListener() {
            @Override
            public void onNoDoubleClick(Component v) {
                if (mDelegate != null) {
                    mDelegate.onClickTextEmptyView(BGAEmptyView.this);
                }
            }
        });
    }

    private void showEmptyView() {
        mContentView.setVisibility(Component.HIDE);
        mEmptyView.setVisibility(Component.VISIBLE);
    }

    public void setDelegate(Delegate delegate) {
        mDelegate = delegate;
    }

    /**
     * 显示文字空视图
     *
     * @param msgResId 提示文字资源 id
     */
    public void showTextEmptyView(int msgResId) {
        showTextEmptyView(getContext().getString(msgResId));
    }

    /**
     * 显示文字空视图
     *
     * @param msg 提示文字
     */
    public void showTextEmptyView(String msg) {
        mIconIv.setVisibility(Component.HIDE);
        mMsgTv.setVisibility(Component.VISIBLE);

        mMsgTv.setText(msg);

        showEmptyView();
    }

    /**
     * 显示图片空视图
     *
     * @param iconResId 图片资源 id
     */
    public void showIconEmptyView(int iconResId) {
        mIconIv.setVisibility(Component.VISIBLE);
        mMsgTv.setVisibility(Component.HIDE);

        Element element = new ShapeElement(getContext(), iconResId);
        mIconIv.setImageElement(element);

        showEmptyView();
    }

    /**
     * 显示文字和图片空视图
     *
     * @param msgResId  提示文字资源 id
     * @param iconResId 图片资源 id
     */
    public void showEmptyView(int msgResId, int iconResId) {
        showEmptyView(getContext().getString(msgResId), iconResId);
    }

    /**
     * 显示文字和图片空视图
     *
     * @param msg       提示文字
     * @param iconResId 图片资源 id
     */
    public void showEmptyView(String msg, int iconResId) {
        mIconIv.setVisibility(Component.VISIBLE);
        mMsgTv.setVisibility(Component.VISIBLE);

        Element element = new ShapeElement(getContext(), iconResId);
        mIconIv.setImageElement(element);
        mMsgTv.setText(msg);

        showEmptyView();
    }

    /**
     * 显示内容视图
     */
    public void showContentView() {
        mEmptyView.setVisibility(Component.HIDE);
        mContentView.setVisibility(Component.VISIBLE);
    }

    public interface Delegate {
        /**
         * 点击了文字空视图
         */
        void onClickTextEmptyView(BGAEmptyView emptyView);

        /**
         * 点击了图片空视图
         */
        void onClickIconEmptyView(BGAEmptyView emptyView);

        /**
         * 点击了空视图
         */
        void onClickEmptyView(BGAEmptyView emptyView);
    }

    public static class SimpleDelegate implements Delegate {
        @Override
        public void onClickTextEmptyView(BGAEmptyView emptyView) {
        }

        @Override
        public void onClickIconEmptyView(BGAEmptyView emptyView) {
        }

        @Override
        public void onClickEmptyView(BGAEmptyView emptyView) {
        }
    }

    /**
     * 查找View
     *
     * @param id   控件的id
     * @param <VT> View类型
     * @return
     */
    protected <VT extends Component> VT getViewById(int id) {
        return (VT) findComponentById(id);
    }
}