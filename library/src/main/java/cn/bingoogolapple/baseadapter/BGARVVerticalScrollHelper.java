/**
 * Copyright 2015 tyzlmjj
 * Copyright 2016 bingoogolapple
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.bingoogolapple.baseadapter;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayoutManager;
import ohos.agp.components.ListContainer;

/**
 * 作者:王浩 邮件:bingoogolapple@gmail.com
 * 创建时间:17/1/9 下午9:27
 * 描述:RecyclerView 滚动到指定位置帮助类。参考的 http://blog.csdn.net/tyzlmjj/article/details/49227601
 */
public class BGARVVerticalScrollHelper implements Component.ScrolledListener {
    private ListContainer mDataRv;
    private Delegate mDelegate;
    private DirectionalLayoutManager mLinearLayoutManager;
    private int mNewPosition = 0;
    private boolean mIsScrolling = false;
    private boolean mIsSmoothScroll = false;
    private int mState = Component.SCROLL_IDLE_STAGE;

    public static BGARVVerticalScrollHelper newInstance(ListContainer recyclerView) {
        return new BGARVVerticalScrollHelper(recyclerView, null);
    }

    public static BGARVVerticalScrollHelper newInstance(ListContainer recyclerView, Delegate delegate) {
        return new BGARVVerticalScrollHelper(recyclerView, delegate);
    }

    private BGARVVerticalScrollHelper(ListContainer recyclerView, Delegate delegate) {
        mDataRv = recyclerView;
        mDataRv.addScrolledListener(this);
        mDelegate = delegate;
    }

    @Override
    public void scrolledStageUpdate(Component component, int newStage) {

        mState = newStage;

        if (newStage == Component.SCROLL_IDLE_STAGE) {
            if (mIsScrolling && mIsSmoothScroll) {
                mIsScrolling = false;
                mIsSmoothScroll = false;

                mDataRv.scrollTo(mNewPosition);
            }
        }

    }

    @Override
    public void onContentScrolled(Component component, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
        if (mState == Component.SCROLL_NORMAL_STAGE) {
            mIsScrolling = false;
            mIsSmoothScroll = false;

            if (mDelegate != null) {
                mDelegate.dragging(findFirstVisibleItemPosition());
            }
        }

        if (!mIsScrolling && !mIsSmoothScroll && mDelegate != null) {
            mDelegate.settling(findFirstVisibleItemPosition());
        }
    }

    private int getCategoryHeight() {
        return mDelegate == null ? 0 : mDelegate.getCategoryHeight();
    }

    public void smoothScrollToPosition(int newPosition) {
        try {
            if (newPosition < 0 || newPosition >= mDataRv.getItemProvider().getCount()) {
                return;
            }

            mNewPosition = newPosition;
            mIsSmoothScroll = true;

            mDataRv.scrollTo(mNewPosition);
            mIsScrolling = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void scrollToPosition(int newPosition) {
        try {
            if (newPosition < 0 || newPosition >= mDataRv.getItemProvider().getCount()) {
                return;
            }

            mNewPosition = newPosition;
            mIsSmoothScroll = false;

            int firstItem = findFirstVisibleItemPosition();
            int lastItem = findLastVisibleItemPosition();
            if (newPosition <= firstItem) {
                mDataRv.scrollTo(newPosition);
            } else if (newPosition <= lastItem) {
                int top = mDataRv.getComponentAt(newPosition).getTop() - mDataRv.getComponentAt(firstItem).getTop() - getCategoryHeight();
                mDataRv.scrollBy(0, top);
            } else {
                mDataRv.scrollTo(newPosition);
               mIsScrolling = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取第一个可见条目索引
     *
     * @return
     */
    public int findFirstVisibleItemPosition() {
        //return getLinearLayoutManager().findFirstVisibleItemPosition();
        return mDataRv.getFirstVisibleItemPosition();
    }

    /**
     * 获取最后一个可见条目索引
     *
     * @return
     */
    public int findLastVisibleItemPosition() {
        //return getLinearLayoutManager().findLastVisibleItemPosition();
        return mDataRv.getLastVisibleItemPosition();
    }

    /**
     * 获取布局管理器
     *
     * @return
     */
    public DirectionalLayoutManager getLinearLayoutManager() {
        if (mLinearLayoutManager == null) {
            mLinearLayoutManager = (DirectionalLayoutManager) mDataRv.getLayoutManager();
        }
        return mLinearLayoutManager;
    }

    public interface Delegate {
        /**
         * 获取分类高度
         *
         * @return
         */
        int getCategoryHeight();

        /**
         * 用户手指拖拽、惯性运动、停止滚动时被调用
         *
         * @param position
         */
        void dragging(int position);

        /**
         * 惯性运动时被调用
         *
         * @param position
         */
        void settling(int position);
    }

    public static class SimpleDelegate implements Delegate {
        @Override
        public int getCategoryHeight() {
            return 0;
        }

        @Override
        public void dragging(int position) {
        }

        @Override
        public void settling(int position) {
        }
    }
}