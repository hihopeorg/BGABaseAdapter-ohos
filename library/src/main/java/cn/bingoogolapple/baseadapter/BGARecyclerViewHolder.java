/**
 * Copyright 2015 bingoogolapple
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.bingoogolapple.baseadapter;

import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.app.Context;

/**
 * 作者:王浩 邮件:bingoogolapple@gmail.com
 * 创建时间:15/5/28 上午7:28
 * 描述:适用于RecyclerView的item的ViewHolder
 */
public class BGARecyclerViewHolder extends Adapter.ViewHolder implements Component.LongClickedListener {
    protected Context mContext;
    protected BGAOnRVItemClickListener mOnRVItemClickListener;
    protected BGAOnRVItemLongClickListener mOnRVItemLongClickListener;
    protected BGAViewHolderHelper mViewHolderHelper;
    protected ListContainer mRecyclerView;
    protected BGARecyclerViewAdapter mRecyclerViewAdapter;

    public BGARecyclerViewHolder(BGARecyclerViewAdapter recyclerViewAdapter, ListContainer recyclerView, Component itemView, BGAOnRVItemClickListener onRVItemClickListener, BGAOnRVItemLongClickListener onRVItemLongClickListener) {
        super(itemView);
        mRecyclerViewAdapter = recyclerViewAdapter;
        mRecyclerView = recyclerView;
        mContext = mRecyclerView.getContext();
        mOnRVItemClickListener = onRVItemClickListener;
        mOnRVItemLongClickListener = onRVItemLongClickListener;
        itemView.setClickedListener(new BGAOnNoDoubleClickListener() {
            @Override
            public void onNoDoubleClick(Component v) {
                if (v.getId() == BGARecyclerViewHolder.this.itemView.getId() && null != mOnRVItemClickListener) {
                    mOnRVItemClickListener.onRVItemClick(mRecyclerView, v, getAdapterPositionWrapper());
                }
            }
        });
        itemView.setLongClickedListener(this);

        mViewHolderHelper = new BGAViewHolderHelper(mRecyclerView, this);
    }

    public BGAViewHolderHelper getViewHolderHelper() {
        return mViewHolderHelper;
    }

    @Override
    public void onLongClicked(Component component) {
        if (component.getId() == this.itemView.getId() && null != mOnRVItemLongClickListener) {
            mOnRVItemLongClickListener.onRVItemLongClick(mRecyclerView, component, getAdapterPositionWrapper());
        }
    }

    public int getAdapterPositionWrapper() {
        if (mRecyclerViewAdapter.getHeadersCount() > 0) {
            return getPosition() - mRecyclerViewAdapter.getHeadersCount();
        } else {
            return getPosition();
        }

    }
}