/**
 * Copyright 2015 bingoogolapple
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cn.bingoogolapple.baseadapter;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.text.Font;
import ohos.agp.text.RichText;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.media.image.PixelMap;
import ohos.miscservices.pasteboard.PasteData;
import ohos.multimodalinput.event.TouchEvent;
import ohos.utils.PlainArray;

import java.io.IOException;

/**
 * 作者:王浩 邮件:bingoogolapple@gmail.com
 * 创建时间:15/5/26 17:06
 * 描述:为AdapterView和RecyclerView的item设置常见属性（链式编程）
 */
public class BGAViewHolderHelper implements Component.LongClickedListener, AbsButton.CheckedStateChangedListener, Component.TouchEventListener {
    protected final PlainArray<Component> mViews;
    protected BGAOnItemChildClickListener mOnItemChildClickListener;
    protected BGAOnItemChildLongClickListener mOnItemChildLongClickListener;
    protected BGAOnItemChildCheckedChangeListener mOnItemChildCheckedChangeListener;
    protected BGAOnRVItemChildTouchListener mOnRVItemChildTouchListener;
    protected Component mConvertView;
    protected Context mContext;
    protected int mPosition;
    protected BGARecyclerViewHolder mRecyclerViewHolder;
    protected ListContainer mRecyclerView;

    //protected AdapterView mAdapterView;
    protected ComponentContainer mAdapterView;
    /**
     * 留着以后作为扩充对象
     */
    protected Object mObj;

    public BGAViewHolderHelper(ComponentContainer adapterView, Component convertView) {
        mViews = new PlainArray<>();
        mAdapterView = adapterView;
        mConvertView = convertView;
        mContext = convertView.getContext();
    }

    public BGAViewHolderHelper(ListContainer recyclerView, BGARecyclerViewHolder recyclerViewHolder) {
        mViews = new PlainArray<>();
        mRecyclerView = recyclerView;
        mRecyclerViewHolder = recyclerViewHolder;
        mConvertView = mRecyclerViewHolder.itemView;
        mContext = mConvertView.getContext();
    }

    public BGARecyclerViewHolder getRecyclerViewHolder() {
        return mRecyclerViewHolder;
    }

    public void setPosition(int position) {
        mPosition = position;
    }

    public int getPosition() {
        if (mRecyclerViewHolder != null) {
            return mRecyclerViewHolder.getAdapterPositionWrapper();
        }
        return mPosition;
    }

    /**
     * 设置item子控件点击事件监听器
     *
     * @param onItemChildClickListener
     */
    public void setOnItemChildClickListener(BGAOnItemChildClickListener onItemChildClickListener) {
        mOnItemChildClickListener = onItemChildClickListener;
    }

    /**
     * 为id为viewId的item子控件设置点击事件监听器
     *
     * @param viewId
     */
    public void setItemChildClickListener(int viewId) {
        Component view = getView(viewId);
        if (view != null) {
            view.setClickedListener(new BGAOnNoDoubleClickListener() {
                @Override
                public void onNoDoubleClick(Component v) {
                    if (mOnItemChildClickListener != null) {
                        if (mRecyclerView != null) {
                            mOnItemChildClickListener.onItemChildClick(mRecyclerView, v, getPosition());
                        } else if (mAdapterView != null) {
                            mOnItemChildClickListener.onItemChildClick(mAdapterView, v, getPosition());
                        }
                    }
                }
            });
        }

    }

    /**
     * 设置item子控件长按事件监听器
     *
     * @param onItemChildLongClickListener
     */
    public void setOnItemChildLongClickListener(BGAOnItemChildLongClickListener onItemChildLongClickListener) {
        mOnItemChildLongClickListener = onItemChildLongClickListener;
    }

    /**
     * 为id为viewId的item子控件设置长按事件监听器
     *
     * @param viewId
     */
    public void setItemChildLongClickListener(int viewId) {
        Component view = getView(viewId);
        if (view != null) {
            view.setLongClickedListener(this);
        }
    }

    /**
     * 设置 RecyclerView 中的 item 子控件触摸事件监听器
     *
     * @param onRVItemChildTouchListener
     */
    public void setOnRVItemChildTouchListener(BGAOnRVItemChildTouchListener onRVItemChildTouchListener) {
        mOnRVItemChildTouchListener = onRVItemChildTouchListener;
    }

    /**
     * 为 id 为 viewId 的 RecyclerView 的 item 子控件设置触摸事件监听器
     *
     * @param viewId
     */
    public void setRVItemChildTouchListener(int viewId) {
        Component view = getView(viewId);
        if (view != null) {
            view.setTouchEventListener(this);
        }
    }

    /**
     * 设置item子控件选中状态变化事件监听器
     *
     * @param onItemChildCheckedChangeListener
     */
    public void setOnItemChildCheckedChangeListener(BGAOnItemChildCheckedChangeListener onItemChildCheckedChangeListener) {
        mOnItemChildCheckedChangeListener = onItemChildCheckedChangeListener;
    }

    /**
     * 为id为viewId的item子控件设置选中状态变化事件监听器
     *
     * @param viewId
     */
    public void setItemChildCheckedChangeListener(int viewId) {
        Component view = getView(viewId);
        if (view != null && view instanceof AbsButton) {
            ((AbsButton) view).setCheckedStateChangedListener(this);
        }
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        if (mOnRVItemChildTouchListener != null && mRecyclerView != null) {
            return mOnRVItemChildTouchListener.onRvItemChildTouch(mRecyclerViewHolder, component, touchEvent);
        }
        return false;
    }

    @Override
    public void onLongClicked(Component v) {
        if (mOnItemChildLongClickListener != null) {
            if (mRecyclerView != null) {
                mOnItemChildLongClickListener.onItemChildLongClick(mRecyclerView, v, getPosition());
            } else if (mAdapterView != null) {
                mOnItemChildLongClickListener.onItemChildLongClick(mAdapterView, v, getPosition());
            }
        }
    }

    @Override
    public void onCheckedChanged(AbsButton absButton, boolean isChecked) {
        if (mOnItemChildCheckedChangeListener != null) {
            if (mRecyclerView != null) {
                BGARecyclerViewAdapter recyclerViewAdapter;

                Adapter adapter = (Adapter) mRecyclerView.getItemProvider();
                if (adapter instanceof BGAHeaderAndFooterAdapter) {
                    recyclerViewAdapter = (BGARecyclerViewAdapter) ((BGAHeaderAndFooterAdapter) adapter).getInnerAdapter();
                } else {
                    recyclerViewAdapter = (BGARecyclerViewAdapter) adapter;
                }
                if (!recyclerViewAdapter.isIgnoreCheckedChanged()) {
                    mOnItemChildCheckedChangeListener.onItemChildCheckedChanged(mRecyclerView, absButton, getPosition(), isChecked);
                }
            } else if (mAdapterView != null) {
                //if (!((BGAAdapterViewAdapter) mAdapterView.getAdapter()).isIgnoreCheckedChanged()) {
                    mOnItemChildCheckedChangeListener.onItemChildCheckedChanged(mAdapterView, absButton, getPosition(), isChecked);
                //}
            }
        }
    }

    /**
     * 通过控件的Id获取对应的控件，如果没有则加入mViews，则从item根控件中查找并保存到mViews中
     *
     * @param viewId
     * @return
     */
    public <T extends Component> T getView(int viewId) {
        Component view = mViews.get(viewId).orElse(null);
        if (view == null) {
            view = mConvertView.findComponentById(viewId);
            mViews.put(viewId, view);
        }
        return (T) view;
    }

    /**
     * 通过ImageView的Id获取ImageView
     *
     * @param viewId
     * @return
     */
    public Image getImageView(int viewId) {
        return getView(viewId);
    }

    /**
     * 通过TextView的Id获取TextView
     *
     * @param viewId
     * @return
     */
    public Text getTextView(int viewId) {
        return getView(viewId);
    }

    /**
     * 获取item的根控件
     *
     * @return
     */
    public Component getConvertView() {
        return mConvertView;
    }

    public void setObj(Object obj) {
        mObj = obj;
    }

    public Object getObj() {
        return mObj;
    }

    /**
     * 设置对应id的控件的文本内容
     *
     * @param viewId
     * @param text
     * @return
     */
    public BGAViewHolderHelper setText(int viewId, String text) {
        if (text == null) {
            text = "";
        }
        getTextView(viewId).setText(text);
        return this;
    }

    /**
     * 设置对应id的控件的文本内容
     *
     * @param viewId
     * @param text
     * @return
     */
    public BGAViewHolderHelper setRichText(int viewId, RichText text) {
        if (text == null) {
            return null;
        }
        getTextView(viewId).setRichText(text);
        return this;
    }

    /**
     * 设置对应id的控件的文本内容
     *
     * @param viewId
     * @param stringResId 字符串资源id
     * @return
     */
    public BGAViewHolderHelper setText(int viewId, int stringResId) {
        getTextView(viewId).setText(stringResId);
        return this;
    }

    /**
     * 设置对应id的控件的文字大小，单位为 sp
     *
     * @param viewId
     * @param size   文字大小，单位为 sp
     * @return
     */
    public BGAViewHolderHelper setTextSizeSp(int viewId, float size) {
        getTextView(viewId).setTextSize(AttrHelper.fp2px(size, 3));
        return this;
    }

    /**
     * 设置对应id的控件的文字是否为粗体
     *
     * @param viewId
     * @param isBold 是否为粗体
     * @return
     */
    public BGAViewHolderHelper setIsBold(int viewId, boolean isBold) {
        getTextView(viewId).setFont(isBold ? Font.DEFAULT_BOLD : Font.DEFAULT);
        return this;
    }

    /**
     * 设置对应id的控件的html文本内容
     *
     * @param viewId
     * @param source html文本
     * @return
     */
    public BGAViewHolderHelper setHtml(int viewId, String source) {
        if (source == null) {
            source = "";
        }
        getTextView(viewId).setText(PasteData.creatHtmlData(source).getPrimaryHtml());
        return this;
    }

    /**
     * 设置对应id的控件是否选中
     *
     * @param viewId
     * @param checked
     * @return
     */
    public BGAViewHolderHelper setChecked(int viewId, boolean checked) {
        AbsButton view = getView(viewId);
        view.setChecked(checked);
        return this;
    }

    public BGAViewHolderHelper setTag(int viewId, Object tag) {
        Component view = getView(viewId);
        view.setTag(tag);
        return this;
    }

    public BGAViewHolderHelper setTag(int viewId, int key, Object tag) {
        Component view = getView(viewId);
        view.setTag(tag);
        return this;
    }

    public BGAViewHolderHelper setVisibility(int viewId, int visibility) {
        Component view = getView(viewId);
        view.setVisibility(visibility);
        return this;
    }

    public BGAViewHolderHelper setImageBitmap(int viewId, PixelMap bitmap) {
        Image view = getView(viewId);
        view.setImageElement(new PixelMapElement(bitmap));
        return this;
    }

    public BGAViewHolderHelper setImageDrawable(int viewId, Element drawable) {
        Image view = getView(viewId);
        view.setImageElement(drawable);
        return this;
    }

    /**
     * @param viewId
     * @param textColorResId 颜色资源id
     * @return
     */
    public BGAViewHolderHelper setTextColorRes(int viewId, int textColorResId) {
        getTextView(viewId).setTextColor(new Color(mContext.getColor(textColorResId)));
        return this;
    }

    /**
     * @param viewId
     * @param textColor 颜色值
     * @return
     */
    public BGAViewHolderHelper setTextColor(int viewId, int textColor) {
        getTextView(viewId).setTextColor(new Color(textColor));
        return this;
    }

    /**
     * @param viewId
     * @param backgroundResId 背景资源id
     * @return
     */
    public BGAViewHolderHelper setBackgroundRes(int viewId, int backgroundResId) {
        Component view = getView(viewId);
        ShapeElement element = new ShapeElement();
        element.setRgbColor(RgbColor.fromArgbInt(mContext.getColor(backgroundResId)));
        view.setBackground(element);
        return this;
    }

    /**
     * @param viewId
     * @param color  颜色值
     * @return
     */
    public BGAViewHolderHelper setBackgroundColor(int viewId, int color) {
        Component view = getView(viewId);
        ShapeElement element = new ShapeElement();
        element.setRgbColor(RgbColor.fromArgbInt(color));
        view.setBackground(element);
        return this;
    }

    /**
     * @param viewId
     * @param colorResId 颜色值资源id
     * @return
     */
    public BGAViewHolderHelper setBackgroundColorRes(int viewId, int colorResId) {
        Component view = getView(viewId);
        ShapeElement element = new ShapeElement();
        element.setRgbColor(new RgbColor(mContext.getColor(colorResId)));
        view.setBackground(element);
        return this;
    }

    /**
     * @param viewId
     * @param imageResId 图像资源id
     * @return
     */
    public BGAViewHolderHelper setImageResource(int viewId, int imageResId) {
        Image view = getView(viewId);
        view.setPixelMap(imageResId);
        return this;
    }

    /**
     * 设置字体是否为粗体
     *
     * @param viewId
     * @param isBold
     * @return
     */
    public BGAViewHolderHelper setBold(int viewId, boolean isBold) {
        getTextView(viewId).setFont(isBold ? Font.DEFAULT_BOLD : Font.DEFAULT);
        return this;
    }
}