# BGABaseAdapter_ohos

本项目是基于开源项目BGABaseAdapter进行ohos化的移植和开发，可以通过项目标签以及github地址（https://github.com/bingoogolapple/BGABaseAdapter-Android ）追踪到原项目版本

#### 项目介绍

- 项目名称：BGABaseAdapter
- 所属系列：ohos的第三方组件适配移植
- 功能：简化ListContainer适配器的编写。
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/bingoogolapple/BGABaseAdapter-Android
- 原项目基线版本：V2.0.1
- 编程语言：Java

#### 演示效果

![Image text](/screenshot/BGABaseAdapter.gif)

#### 安装教程
方法1.
1. 下载har包BGABaseAdapter_ohos.har。
2. 启动 DevEco Studio，将下载的har包，导入工程目录“entry->libs”下。
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。
```
repositories {
    flatDir { dirs 'libs' }
}
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    implementation(name: 'BGABaseAdapter_ohos', ext: 'har')
	……
}
```
方法2.
1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址
```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/'
    }
}
```
2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:
```
dependencies {
    implementation 'com.bingoogolapple.ohos:BGABaseAdapter-ohos:1.0.0'
}
```

#### 使用说明

1. 我们只需要创建适配器类继承自BGARecyclerViewAdapter。

```java
public class RvChatAdapter extends BGARecyclerViewAdapter<ChatModel> {

    public RvChatAdapter(ListContainer listContainer) {
        super(listContainer, ResourceTable.Layout_item_cascade_goods);
    }

    @Override
    public void fillData(BGAViewHolderHelper helper, int position, ChatModel model) {
        //UI操作，设置点击事件监听等
        helper.setText(ResourceTable.Id_tv_item_chat_other_msg,"");
    }
    
    ....
}
```
2. 设置适配器

```java
ListContainer mDataRv = (ListContainer)findComponentById(ResourceTable.Id_rv_data);
RvChatAdapter mAdapter = new RvChatAdapter(mDataRv);
mAdapter.setData(datalist);
mDataRv.setItemProvider(mAdapter);
```

#### 版本迭代

- v1.0.0

#### 版权和许可信息

```
Copyright 2015 bingoogolapple

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```