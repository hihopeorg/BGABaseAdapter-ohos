#### version 1.0.1

1. 优化吸顶效果适配。
2. 添加lv，gv界面。

#### version 1.0.0

1. 简化 ListContainer 的适配器的编写，支持多item类型及各item的控件点击监听。
2. BGARVVerticalScrollHelper 用于将 ListContainer 滚动到指定位置，支持吸顶悬浮索引。

- 不支持：

1. 不支持间隔线。
2. item拖动交换及滑动事件。


