package cn.bingoogolapple.baseadapter.demo;

import cn.bingoogolapple.baseadapter.BGARVVerticalScrollHelper;
import cn.bingoogolapple.baseadapter.demo.ui.fraction.RvCascadeFraction;
import cn.bingoogolapple.baseadapter.demo.utils.EventHelper;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.DirectionalLayoutManager;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;

import static org.junit.Assert.*;

public class BGARVVerticalScrollHelperTest {

    private BGARVVerticalScrollHelper scrollHelper;

    @Before
    public void setUp() throws Exception {
        MainAbility mainAbility = (MainAbility) EventHelper.startAbility(MainAbility.class);
        assertNotNull("testShowExample failed, can not start ability", mainAbility);
        AbilitySlice currentAbilitySlice = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentAbilitySlice(mainAbility);
        assertNotNull("can not get AbilitySlice", currentAbilitySlice);
        Thread.sleep(3000);
        Field f = currentAbilitySlice.getClass().getDeclaredField("mRvCascadeFraction");
        f.setAccessible(true);
        RvCascadeFraction mRvCascadeFraction = (RvCascadeFraction)f.get(currentAbilitySlice);
        Field helper = mRvCascadeFraction.getClass().getDeclaredField("mGoodsScrollHelper");
        helper.setAccessible(true);
        scrollHelper = (BGARVVerticalScrollHelper) helper.get(mRvCascadeFraction);

    }

    @Test
    public void newInstance() {
        assertNotNull(scrollHelper);
    }

    @Test
    public void smoothScrollToPosition() throws NoSuchFieldException, IllegalAccessException {
        scrollHelper.smoothScrollToPosition(29);
        Field field = scrollHelper.getClass().getDeclaredField("mNewPosition");
        field.setAccessible(true);
        assertEquals(29, field.get(scrollHelper));
    }

    @Test
    public void getLinearLayoutManager() {
        DirectionalLayoutManager directionalLayoutManager = scrollHelper.getLinearLayoutManager();
        assertNotNull(directionalLayoutManager);
    }
}