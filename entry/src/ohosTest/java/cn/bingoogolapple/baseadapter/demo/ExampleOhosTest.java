package cn.bingoogolapple.baseadapter.demo;

import cn.bingoogolapple.baseadapter.BGARVVerticalScrollHelper;
import cn.bingoogolapple.baseadapter.BGARecyclerViewAdapter;
import cn.bingoogolapple.baseadapter.demo.slice.MainAbilitySlice;
import cn.bingoogolapple.baseadapter.demo.ui.fraction.RvCascadeFraction;
import cn.bingoogolapple.baseadapter.demo.ui.fraction.RvChatFraction;
import cn.bingoogolapple.baseadapter.demo.ui.widget.IndexView;
import cn.bingoogolapple.baseadapter.demo.utils.EventHelper;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.*;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.lang.reflect.Field;

import static org.junit.Assert.assertEquals;

public class ExampleOhosTest {
    private static Ability ability = EventHelper.startAbility(MainAbility.class);
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00102, "BGABaseAdapter");
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("cn.bingoogolapple.baseadapter.demo", actualBundleName);
    }

    @Test
    public void test01(){
        TabList tabList = (TabList)ability.findComponentById(ResourceTable.Id_tabList);
        TabList.Tab tabAt = tabList.getTabAt(0);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability,tabAt);
        threadSleep(3000);
        AbilitySlice currentAbilitySlice = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentAbilitySlice(ability);
        exec("input tap 100 1500");
        threadSleep(3000);
        RvCascadeFraction mRvCascadeFraction=null;
        try {
            Field f = MainAbilitySlice.class.getDeclaredField("mRvCascadeFraction");
            f.setAccessible(true);
            mRvCascadeFraction = (RvCascadeFraction)f.get(currentAbilitySlice);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        BGARVVerticalScrollHelper mGoodsScrollHelper=null;
        try {
            Field f = RvCascadeFraction.class.getDeclaredField("mGoodsScrollHelper");
            f.setAccessible(true);
            mGoodsScrollHelper = (BGARVVerticalScrollHelper)f.get(mRvCascadeFraction);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        ListContainer mGoodsRv=null;
        try {
            Field f = RvCascadeFraction.class.getDeclaredField("mGoodsRv");
            f.setAccessible(true);
            mGoodsRv = (ListContainer)f.get(mRvCascadeFraction);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        ListContainer mCategoryRv = null;
        try {
            Field f = RvCascadeFraction.class.getDeclaredField("mCategoryRv");
            f.setAccessible(true);
            mCategoryRv = (ListContainer) f.get(mRvCascadeFraction);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        //商品数量
        int childCount = mGoodsRv.getChildCount();
        Assert.assertTrue("显示不出商品",childCount==81);
        //点击商品目录
        BGARecyclerViewAdapter itemProvider = (BGARecyclerViewAdapter)mCategoryRv.getItemProvider();
        int checkedPosition = itemProvider.getCheckedPosition();
        Assert.assertTrue("点击事件后的分类目录未处于选中状态",checkedPosition>0);
        DirectionalLayout directionalLayout00 = (DirectionalLayout) mCategoryRv.getComponentAt(checkedPosition);
        String s = String.valueOf(checkedPosition);
        Text text = (Text) directionalLayout00.getComponentAt(0);
        HiLog.warn(LABEL, "Text:" + text.getText());
        int firstVisibleItemPosition = mGoodsScrollHelper.findFirstVisibleItemPosition();
        DirectionalLayout directionalLayout01 = (DirectionalLayout)mGoodsRv.getComponentAt(firstVisibleItemPosition);
        Text text00 = (Text)directionalLayout01.getComponentAt(1);
        String text01 = text00.getText();
        HiLog.warn(LABEL,"商品列表:"+text01);
        String substring = text01.substring(2, 3);
        Assert.assertTrue("点击分类目录没有和商品列表对应",s.equals(substring));
        //滑动商品列表
        exec("input swipe 800 500 800 1500");
        threadSleep(6000);
        int checkedPosition01 = itemProvider.getCheckedPosition();
        Assert.assertTrue("点击事件后的分类目录未处于选中状态",checkedPosition01>0);
        DirectionalLayout directionalLayout02 = (DirectionalLayout) mCategoryRv.getComponentAt(checkedPosition01);
        String ss = String.valueOf(checkedPosition01);
        Text text02 = (Text) directionalLayout02.getComponentAt(0);
        HiLog.warn(LABEL, "Text:" + text02.getText());
        int firstVisibleItemPosition00 = mGoodsScrollHelper.findFirstVisibleItemPosition();
        DirectionalLayout directionalLayout03 = (DirectionalLayout)mGoodsRv.getComponentAt(firstVisibleItemPosition00);
        Text text03 = (Text)directionalLayout03.getComponentAt(1);
        String text04 = text03.getText();
        HiLog.warn(LABEL,"商品列表:"+text04);
        String substring01 = text04.substring(2, 3);
        Assert.assertTrue("滑动商品列表没有和分类目录对应",ss.equals(substring01));
    }

    @Test
    public void test02() {
        TabList tabList = (TabList)ability.findComponentById(ResourceTable.Id_tabList);
        TabList.Tab tabAt = tabList.getTabAt(1);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability,tabAt);
        threadSleep(4000);
        StackLayout stackLayout = (StackLayout)ability.findComponentById(ResourceTable.Id_stackLayout);
        StackLayout stackLayout01 = (StackLayout)stackLayout.getComponentAt(1);
        Text text = (Text)stackLayout01.getComponentAt(1);
        HiLog.warn(LABEL,"右边A:"+text.getText());
        ListContainer listContainer = (ListContainer)stackLayout01.getComponentAt(0);
        Text text00 = (Text)listContainer.getComponentAt(26);
        HiLog.warn(LABEL,"左边城市不在页面内:"+text00);
        exec("input tap 1050 1050");
        exec("input tap 1050 1050");
        threadSleep(4000);
        IndexView indexView = (IndexView)stackLayout01.getComponentAt(2);
        Text mTipTv=null;
        try {
            Field f = IndexView.class.getDeclaredField("mTipTv");
            f.setAccessible(true);
            mTipTv = (Text)f.get(indexView);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        HiLog.warn(LABEL,"点击右边导航:"+mTipTv.getText());
        ListContainer listContainer01 = (ListContainer)stackLayout01.getComponentAt(0);
        Text text01 = (Text)listContainer01.getComponentAt(26);
        HiLog.warn(LABEL,"左边城市:"+text01.getText());
        Assert.assertTrue("右边点击J，未导航到济南",text00!=text01);
    }

    @Test
    public void test03(){
        TabList tabList = (TabList)ability.findComponentById(ResourceTable.Id_tabList);
        TabList.Tab tabAt = tabList.getTabAt(2);
        AbilityDelegatorRegistry.getAbilityDelegator().triggerClickEvent(ability,tabAt);
        threadSleep(3000);
        AbilitySlice currentAbilitySlice = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentAbilitySlice(ability);
        threadSleep(3000);
        RvChatFraction mRvChatFraction=null;
        try {
            Field f = MainAbilitySlice.class.getDeclaredField("mRvChatFraction");
            f.setAccessible(true);
            mRvChatFraction = (RvChatFraction)f.get(currentAbilitySlice);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        ListContainer mDataRv=null;
        try {
            Field f = RvChatFraction.class.getDeclaredField("mDataRv");
            f.setAccessible(true);
            mDataRv = (ListContainer)f.get(mRvChatFraction);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        int childCount = mDataRv.getChildCount();
        HiLog.warn(LABEL,"对话框数量:"+childCount);
        Assert.assertTrue("页面中无三十条信息",childCount==30);
    }

    public void threadSleep(int x) {
        try {
            Thread.sleep(x);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void exec(String x){
        try {
            Runtime.getRuntime().exec(x);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}