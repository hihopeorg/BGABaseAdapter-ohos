package cn.bingoogolapple.baseadapter.demo.ui.widget;

import cn.bingoogolapple.baseadapter.demo.ResourceTable;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

public class IndexView extends Component implements Component.DrawTask, Component.TouchEventListener {
    public static final String[] mData = {"A", "B", "C", "D", "E", "F", "G", "H", "I",
            "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
            "W", "X", "Y", "Z"};

    private int mSelected = -1;
    private Paint mPaint = new Paint();
    private Delegate mDelegate;
    private Text mTipTv;
    private int mNormalTextColor;
    private int mPressedTextColor;

    public IndexView(Context context) {
        this(context, null);
    }

    public IndexView(Context context, AttrSet attrs) {
        this(context, attrs, 0);
    }

    public IndexView(Context context, AttrSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        mNormalTextColor = context.getColor(ResourceTable.Color_colorPrimary);
        mPressedTextColor = context.getColor(ResourceTable.Color_colorPrimaryDark);

        mPaint.setFont(Font.DEFAULT);
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setTextSize(sp2px(14));

        setTouchEventListener(this);
        addDrawTask(this);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        int width = getWidth();
        int singleHeight = getHeight() / mData.length;

        for (int i = 0; i < mData.length; i++) {
            mPaint.setColor(new Color(mNormalTextColor));
            mPaint.setFont(Font.DEFAULT);
            if (i == mSelected) {
                mPaint.setColor(new Color(mPressedTextColor));
                mPaint.setFont(Font.DEFAULT_BOLD);
            }
            float xPos = width / 2 - mPaint.measureText(mData[i]) / 2;
            float yPos = singleHeight * i + singleHeight;
            canvas.drawText(mPaint, mData[i], xPos, yPos);
        }
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent event) {
        MmiPoint point = event.getPointerScreenPosition(event.getIndex());
        int [] location = component.getLocationOnScreen();
        int newSelected = (int) ((point.getY() - location[1]) / getHeight() * mData.length);
        switch (event.getAction()) {
            case TouchEvent.PRIMARY_POINT_UP:
            case TouchEvent.CANCEL:
                mSelected = -1;
                invalidate();
                if (mTipTv != null) {
                    mTipTv.setVisibility(Component.INVISIBLE);
                }
                break;

            default:
                if (mSelected != newSelected) {
                    if (newSelected >= 0 && newSelected < mData.length) {
                        if (mDelegate != null) {
                            mDelegate.onIndexViewSelectedChanged(this, mData[newSelected]);
                        }
                        if (mTipTv != null) {
                            mTipTv.setText(mData[newSelected]);
                            mTipTv.setVisibility(Component.VISIBLE);
                        }
                        mSelected = newSelected;
                        invalidate();
                    }
                }
                break;
        }
        return true;
    }

    public int sp2px(float spValue) {
        return AttrHelper.fp2px(spValue, getContext());
    }

    public void setDelegate(Delegate delegate) {
        mDelegate = delegate;
    }

    public void setTipTv(Text tipTv) {
        mTipTv = tipTv;
    }

    public interface Delegate {
        void onIndexViewSelectedChanged(IndexView indexView, String text);
    }

}