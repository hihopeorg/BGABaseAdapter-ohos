package cn.bingoogolapple.baseadapter.demo.adapter;

import cn.bingoogolapple.baseadapter.demo.ResourceTable;
import cn.bingoogolapple.baseadapter.demo.model.NormalModel;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.TextAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;

import java.util.List;

public class RvAdapter extends BaseItemProvider {

    private Context mContext;
    private List<NormalModel> mData;

    public RvAdapter(Context context, List<NormalModel> data) {
        super();
        mContext = context;
        mData = data;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Object getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        if (position == 0) {
            Image image = new Image(mContext);
            image.setWidth(ComponentContainer.LayoutConfig.MATCH_PARENT);
            image.setScaleMode(Image.ScaleMode.STRETCH);
            image.setPixelMap(ResourceTable.Media_holder_banner);
            return image;
        }else if (position == 1) {
            Text text = initText();
            ShapeElement element = new ShapeElement();
            element.setRgbColor(RgbColor.fromArgbInt(Color.getIntColor("#E15B5A")));
            text.setBackground(element);
            text.setText("头部1");
            text.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    mData.add(2, new NormalModel("NewTitle", "NewDetail", false));
                    mData.add(3, new NormalModel("NewTitle", "NewDetail", false));
                    mData.add(4, new NormalModel("NewTitle", "NewDetail", false));
                    //notifyDataSetItemRangeInserted(2, 3);
                    notifyDataChanged();
                    new ToastDialog(mContext).setText("点击了头部1").setDuration(1000).show();
                }
            });
            return text;
        } else if (position == getCount() - 2) {
            Text text = initText();
            ShapeElement element = new ShapeElement();
            element.setRgbColor(RgbColor.fromArgbInt(Color.getIntColor("#6C9FFC")));
            text.setBackground(element);
            text.setText("底部1");
            text.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    mData.add(getCount() - 1, new NormalModel("NewTitle", "NewDetail", false));
                    mData.add(getCount() - 2, new NormalModel("NewTitle", "NewDetail", false));
                    mData.add(getCount() - 3, new NormalModel("NewTitle", "NewDetail", false));
                    notifyDataChanged();
                    new ToastDialog(mContext).setText("点击了底部1").setDuration(1000).show();
                }
            });
            return text;
        } else if (position == getCount() - 1) {
            Text text = initText();
            ShapeElement element = new ShapeElement();
            element.setRgbColor(RgbColor.fromArgbInt(Color.getIntColor("#51535B")));
            text.setBackground(element);
            text.setText("底部2");
            text.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    new ToastDialog(mContext).setText("点击了底部2").setDuration(1000).show();
                }
            });
            return text;
        } else {
            Component cpt = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_item_binding_normal, null, false);
            Switch button = (Switch) cpt.findComponentById(ResourceTable.Id_cb_item_normal_status);
            Text text = (Text) cpt.findComponentById(ResourceTable.Id_delete);
            button.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
                @Override
                public void onCheckedChanged(AbsButton absButton, boolean isChecked) {
                    if (mData.get(position).getSelected() == isChecked) {
                        return;
                    }
                    mData.get(position).setSelected(isChecked);
                }
            });
            text.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    mData.remove(position);
                    notifyDataSetItemRemoved(position);
                    notifyDataChanged();
                }
            });
            button.setChecked(mData.get(position).getSelected());
            return cpt;
        }
    }

    private Text initText() {
        Text text = new Text(mContext);
        text.setWidth(ComponentContainer.LayoutConfig.MATCH_PARENT);
        text.setHeight(ComponentContainer.LayoutConfig.MATCH_CONTENT);
        text.setTextColor(Color.WHITE);
        text.setTextAlignment(TextAlignment.CENTER);
        text.setPadding(30, 30, 30, 30);
        text.setTextSize(16, Text.TextSizeType.FP);
        return text;
    }
}
