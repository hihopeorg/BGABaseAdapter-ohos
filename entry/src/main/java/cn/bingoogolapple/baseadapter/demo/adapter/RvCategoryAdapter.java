package cn.bingoogolapple.baseadapter.demo.adapter;

import cn.bingoogolapple.baseadapter.BGARecyclerViewAdapter;
import cn.bingoogolapple.baseadapter.BGAViewHolderHelper;
import cn.bingoogolapple.baseadapter.demo.ResourceTable;
import cn.bingoogolapple.baseadapter.demo.model.CategoryModel;
import ohos.agp.components.ListContainer;

/**
 * 作者:王浩 邮件:bingoogolapple@gmail.com
 * 创建时间:15/5/22 16:31
 * 描述:仿美团外卖点餐界面左右联动-商品分类列表适配器
 */
public class RvCategoryAdapter extends BGARecyclerViewAdapter<CategoryModel> {

    public RvCategoryAdapter(ListContainer recyclerView) {
        super(recyclerView, ResourceTable.Layout_item_cascade_category);
    }

    @Override
    public void fillData(BGAViewHolderHelper helper, int position, CategoryModel model) {
        // 设置选中和未选中的背景
        if (mCheckedPosition == position) {
            helper.setBackgroundColorRes(ResourceTable.Id_tv_item_cascade_category_name, ResourceTable.Color_white);
        } else {
            helper.setBackgroundRes(ResourceTable.Id_tv_item_cascade_category_name, ResourceTable.Color_bga_adapter_item_pressed);
        }
        helper.setText(ResourceTable.Id_tv_item_cascade_category_name, model.name);
    }

    /**
     * 根据分类id获取分类索引
     *
     * @param categoryId
     * @return
     */
    public int getPositionByCategoryId(int categoryId) {
        if (categoryId < 0) {
            return 0;
        }

        int itemCount = getCount();
        for (int i = 0; i < itemCount; i++) {
            if (getItem(i).id == categoryId) {
                return i;
            }
        }
        return 0;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }
}