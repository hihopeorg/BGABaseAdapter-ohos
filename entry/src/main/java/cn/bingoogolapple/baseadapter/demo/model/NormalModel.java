package cn.bingoogolapple.baseadapter.demo.model;

/**
 * 作者:王浩 邮件:bingoogolapple@gmail.com
 * 创建时间:15/5/21 14:53
 * 描述:
 */
public class NormalModel {
    public String title;
    public String detail;
    public boolean selected;
    public int res;

    public NormalModel() {
    }

    public NormalModel(String title, String detail, boolean selected, int res) {
        this.title = title;
        this.detail = detail;
        this.selected = selected;
        this.res = res;
    }

    public NormalModel(String title, String detail, boolean selected) {
        this.title = title;
        this.detail = detail;
        this.selected = selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean getSelected() {
        return selected;
    }
}