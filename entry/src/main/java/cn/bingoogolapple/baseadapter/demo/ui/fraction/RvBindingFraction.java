package cn.bingoogolapple.baseadapter.demo.ui.fraction;

import cn.bingoogolapple.baseadapter.demo.ResourceTable;
import cn.bingoogolapple.baseadapter.demo.adapter.RvAdapter;
import cn.bingoogolapple.baseadapter.demo.model.NormalModel;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.window.dialog.ToastDialog;

import java.util.ArrayList;
import java.util.List;

public class RvBindingFraction extends Fraction {

    private ListContainer listContainer;
    private List<NormalModel> mData = new ArrayList<>();
    private RvAdapter mRvAdapter;


    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        Component component = scatter.parse(ResourceTable.Layout_fraction_rv, null, false);
        listContainer = (ListContainer) component.findComponentById(ResourceTable.Id_listContainer);
        return component;
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        initData();
        mRvAdapter = new RvAdapter(getFractionAbility().getContext(), mData);
        listContainer.setItemProvider(mRvAdapter);
        listContainer.setItemClickedListener(new ListContainer.ItemClickedListener() {
            @Override
            public void onItemClicked(ListContainer listContainer, Component component, int position, long l) {
                if (position >= 2 && position < mRvAdapter.getCount() - 2) {
                    new ToastDialog(getFractionAbility().getContext()).setText("点击了item" + (position - 2)).setDuration(1000).show();
                }
            }
        });

        listContainer.setItemLongClickedListener(new ListContainer.ItemLongClickedListener() {
            @Override
            public boolean onItemLongClicked(ListContainer listContainer, Component component, int position, long l) {
                if (position >= 2 && position < mRvAdapter.getCount() - 2) {
                    new ToastDialog(getFractionAbility().getContext()).setText("长按了item" + (position - 2)).setDuration(1000).show();
                }
                return false;
            }
        });
    }

    private void initData() {
        mData.add(new NormalModel("NewTitle", "NewDetail", false));
        mData.add(new NormalModel("NewTitle", "NewDetail", false));
        mData.add(new NormalModel("NewTitle", "NewDetail", false));
        mData.add(new NormalModel("NewTitle", "NewDetail", false));
    }
}
