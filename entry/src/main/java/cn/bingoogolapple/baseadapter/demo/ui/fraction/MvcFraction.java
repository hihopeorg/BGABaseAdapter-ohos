package cn.bingoogolapple.baseadapter.demo.ui.fraction;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;

/**
 * 作者:王浩 邮件:bingoogolapple@gmail.com
 * 创建时间:15/6/28 下午12:30
 * 描述:
 */
public abstract class MvcFraction extends Fraction {
    protected Component mContentView;
    protected Ability mAbility;

    protected boolean mIsLoadedData = false;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        mAbility = getFractionAbility();
    }

    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        // 避免多次从xml中加载布局文件
        if (mContentView == null) {
            mContentView = scatter.parse(getRootLayoutResID(), container, false);
            initView();
            setListener();
            container.getContext().getUITaskDispatcher().delayDispatch(()->{processLogic();},200);
            //processLogic();
        } else {
            ComponentContainer parent = (ComponentContainer) mContentView.getComponentParent();
            if (parent != null) {
                parent.removeComponent(mContentView);
            }
        }
        return mContentView;
    }

    @Override
    protected void onActive() {
        super.onActive();
        handleOnVisibilityChangedToUser(true);
    }

    @Override
    protected void onInactive() {
        super.onInactive();
        handleOnVisibilityChangedToUser(false);
    }

    /**
     * 处理对用户是否可见
     *
     * @param isVisibleToUser
     */
    private void handleOnVisibilityChangedToUser(boolean isVisibleToUser) {
        if (isVisibleToUser) {
            // 对用户可见
            if (!mIsLoadedData) {
                mIsLoadedData = true;
                onLazyLoadOnce();
            }
            onVisibleToUser();
        } else {
            // 对用户不可见
            onInvisibleToUser();
        }
    }

    /**
     * 懒加载一次。如果只想在对用户可见时才加载数据，并且只加载一次数据，在子类中重写该方法
     */
    protected void onLazyLoadOnce() {
    }

    /**
     * 对用户可见时触发该方法。如果只想在对用户可见时才加载数据，在子类中重写该方法
     */
    protected void onVisibleToUser() {
    }

    /**
     * 对用户不可见时触发该方法
     */
    protected void onInvisibleToUser() {
    }

    /**
     * 获取布局文件根视图
     *
     * @return
     */
    protected abstract int getRootLayoutResID();

    /**
     * 初始化 View 控件
     */
    protected abstract void initView();

    /**
     * 给 View 控件添加事件监听器
     */
    protected abstract void setListener();

    /**
     * 处理业务逻辑，状态恢复等操作
     */
    protected abstract void processLogic();

    /**
     * 查找View
     *
     * @param id   控件的id
     * @param <VT> View类型
     * @return
     */
    protected <VT extends Component> VT getViewById(int id) {
        return (VT) mContentView.findComponentById(id);
    }

}