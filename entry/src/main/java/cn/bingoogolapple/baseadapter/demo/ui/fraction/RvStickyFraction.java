package cn.bingoogolapple.baseadapter.demo.ui.fraction;

import cn.bingoogolapple.baseadapter.BGADivider;
import cn.bingoogolapple.baseadapter.BGAOnRVItemClickListener;
import cn.bingoogolapple.baseadapter.BGARVVerticalScrollHelper;
import cn.bingoogolapple.baseadapter.demo.ResourceTable;
import cn.bingoogolapple.baseadapter.demo.adapter.RvStickyAdapter;
import cn.bingoogolapple.baseadapter.demo.engine.DataEngine;
import cn.bingoogolapple.baseadapter.demo.ui.widget.IndexView;
import ohos.agp.components.*;

public class RvStickyFraction extends MvcFraction implements BGAOnRVItemClickListener {
    private RvStickyAdapter mAdapter;
    private ListContainer mDataRv;
    private IndexView mIndexView;
    private Text mTipTv;
    private BGARVVerticalScrollHelper mRecyclerViewScrollHelper;

    @Override
    protected int getRootLayoutResID() {
        return ResourceTable.Layout_fraction_rv_sticky;
    }

    @Override
    protected void initView() {
        mDataRv = getViewById(ResourceTable.Id_rv_sticky_data);

        mIndexView = getViewById(ResourceTable.Id_iv_sticky_index);
        mTipTv = getViewById(ResourceTable.Id_tv_sticky_tip);
    }

    @Override
    protected void setListener() {
        mAdapter = new RvStickyAdapter(mDataRv);
        mAdapter.setOnRVItemClickListener(this);

        initStickyDivider();

        mIndexView.setDelegate(new IndexView.Delegate() {
            @Override
            public void onIndexViewSelectedChanged(IndexView indexView, String text) {
                int position = mAdapter.getPositionForCategory(text.charAt(0));
                if (position != -1) {
                    mRecyclerViewScrollHelper.scrollToPosition(position);
                }
            }
        });
    }

    private void initStickyDivider() {
        final BGADivider.StickyDelegate stickyDelegate = new BGADivider.StickyDelegate() {
            @Override
            public void initCategoryAttr() {
            }

            @Override
            protected boolean isCategoryFistItem(int position) {
                return mAdapter.isCategoryFistItem(position);
            }

            @Override
            protected String getCategoryName(int position) {
                return mAdapter.getItem(position).topc;
            }

            @Override
            protected int getFirstVisibleItemPosition() {
                return mRecyclerViewScrollHelper.findFirstVisibleItemPosition();
            }
        };

        mRecyclerViewScrollHelper = BGARVVerticalScrollHelper.newInstance(mDataRv, new BGARVVerticalScrollHelper.SimpleDelegate() {
            @Override
            public int getCategoryHeight() {
                return stickyDelegate.getCategoryHeight();
            }
        });
    }

    @Override
    protected void processLogic() {
        mIndexView.setTipTv(mTipTv);

        mAdapter.setData(DataEngine.loadIndexModelData());
        mDataRv.setLongClickable(false);
        mDataRv.setItemProvider(mAdapter);
    }

    @Override
    public void onRVItemClick(ComponentContainer parent, Component itemView, int position) {
    }
}
