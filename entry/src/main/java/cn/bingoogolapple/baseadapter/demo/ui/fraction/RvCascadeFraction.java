package cn.bingoogolapple.baseadapter.demo.ui.fraction;

import java.util.ArrayList;
import java.util.List;

import cn.bingoogolapple.baseadapter.BGADivider;
import cn.bingoogolapple.baseadapter.BGAOnRVItemClickListener;
import cn.bingoogolapple.baseadapter.BGARVVerticalScrollHelper;
import cn.bingoogolapple.baseadapter.demo.ResourceTable;
import cn.bingoogolapple.baseadapter.demo.adapter.RvCategoryAdapter;
import cn.bingoogolapple.baseadapter.demo.adapter.RvGoodsAdapter;
import cn.bingoogolapple.baseadapter.demo.engine.DataEngine;
import cn.bingoogolapple.baseadapter.demo.model.CategoryModel;
import cn.bingoogolapple.baseadapter.demo.model.GoodsModel;
import ohos.agp.components.*;

/**
 * 作者:王浩 邮件:bingoogolapple@gmail.com
 * 创建时间:15/5/22 10:06
 * 描述:仿美团外卖点餐界面左右联动
 */
public class RvCascadeFraction extends MvcFraction {
    private ListContainer mCategoryRv;
    private ListContainer mGoodsRv;
    private RvCategoryAdapter mCategoryAdapter;
    private RvGoodsAdapter mGoodsAdapter;
    private BGARVVerticalScrollHelper mGoodsScrollHelper; // 商品列表滚动帮助类

    @Override
    protected int getRootLayoutResID() {
        return ResourceTable.Layout_fraction_rv_cascade;
    }

    @Override
    protected void initView() {
        mCategoryRv = getViewById(ResourceTable.Id_rv_cascade_category);
        mGoodsRv = getViewById(ResourceTable.Id_rv_cascade_goods);
    }

    @Override
    protected void setListener() {
        // 分类
        mCategoryAdapter = new RvCategoryAdapter(mCategoryRv);
        mCategoryAdapter.setOnRVItemClickListener(new BGAOnRVItemClickListener() {
            @Override
            public void onRVItemClick(ComponentContainer parent, Component itemView, int position) {
                mCategoryAdapter.setCheckedPosition(position);
                long categoryId = mCategoryAdapter.getItem(position).id;
                int goodsPosition = mGoodsAdapter.getFirstPositionByCategoryId(categoryId);

                mGoodsScrollHelper.smoothScrollToPosition(goodsPosition);
//                mGoodsScrollHelper.scrollToPosition(goodsPosition);
            }
        });
        mCategoryRv.setLongClickable(false);
        mCategoryRv.setItemProvider(mCategoryAdapter);


        // 商品
        final BGADivider.StickyDelegate stickyDelegate = new BGADivider.StickyDelegate() {
            @Override
            protected boolean isCategoryFistItem(int position) {
                return mGoodsAdapter.isCategoryFistItem(position);
            }

            @Override
            protected String getCategoryName(int position) {
                int categoryId = mGoodsAdapter.getItem(position).categoryId;
                int categoryPosition = mCategoryAdapter.getPositionByCategoryId(categoryId);
                return mCategoryAdapter.getItem(categoryPosition).name;
            }

            @Override
            protected int getFirstVisibleItemPosition() {
                return mGoodsScrollHelper.findFirstVisibleItemPosition();
            }
        };

        mGoodsScrollHelper = BGARVVerticalScrollHelper.newInstance(mGoodsRv, new BGARVVerticalScrollHelper.SimpleDelegate() {
            @Override
            public int getCategoryHeight() {
                return stickyDelegate.getCategoryHeight();
            }

            @Override
            public void dragging(int position) {
                int categoryId = mGoodsAdapter.getItem(position).categoryId;
                int categoryPosition = mCategoryAdapter.getPositionByCategoryId(categoryId);
                mCategoryAdapter.setCheckedPosition(categoryPosition);

                //mCategoryRv.scrollToCenter(mCategoryAdapter.getCheckedPosition());
                mCategoryRv.scrollTo(categoryPosition);
            }

            @Override
            public void settling(int position) {
                this.dragging(position);
            }
        });
        mGoodsAdapter = new RvGoodsAdapter(mGoodsRv);
        mGoodsAdapter.setOnRVItemClickListener(new BGAOnRVItemClickListener() {
            @Override
            public void onRVItemClick(ComponentContainer parent, Component itemView, int position) {

            }
        });
        mGoodsRv.setLongClickable(false);
        mGoodsRv.setItemProvider(mGoodsAdapter);
    }

    @Override
    protected void processLogic() {
        List<CategoryModel> categoryModelList = DataEngine.loadCategoryData();
        List<GoodsModel> goodsModelList = new ArrayList<>();
        for (CategoryModel categoryModel : categoryModelList) {
            goodsModelList.addAll(categoryModel.goodsModelList);
        }
        mCategoryAdapter.setData(categoryModelList);
        mGoodsAdapter.setData(goodsModelList);

    }
}