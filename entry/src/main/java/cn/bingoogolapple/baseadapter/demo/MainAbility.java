package cn.bingoogolapple.baseadapter.demo;

import cn.bingoogolapple.baseadapter.demo.ui.fraction.*;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.ability.fraction.FractionManager;
import ohos.aafwk.ability.fraction.FractionScheduler;
import ohos.aafwk.content.Intent;
import ohos.agp.components.TabList;
import ohos.utils.PacMap;
import ohos.utils.PlainArray;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MainAbility extends FractionAbility {

    private TabList mTabList;
    private FractionManager mFractionManager;
    private RvCascadeFraction mRvCascadeFraction;
    private RvStickyFraction mRvStickyFraction;
    private RvChatFraction mRvChatFraction;
    private RvBindingFraction mRvBindingFraction;
    private LvFraction mLvFraction;
    private GvFraction mGvFraction;

    private Fraction mCurFraction;
    private List<Fraction> mFractions = new ArrayList<>();
    private PlainArray<Fraction> mFractionPlainArray = new PlainArray<>();
    TabList.Tab rvCascadeTab, rvStickyTab, rvChatTab, rvBingingTab, lvTab, gvTab;
    private int position = 0;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        getWindow().setStatusBarColor(getColor(ResourceTable.Color_colorPrimary));
        initManager();
        initTab();
    }

    @Override
    public void onSaveAbilityState(PacMap outState) {
        super.onSaveAbilityState(outState);
        outState.putIntValue("position", position);
    }

    @Override
    protected void onBackground() {
        super.onBackground();
        mFractionManager.startFractionScheduler()
                .remove(mRvBindingFraction).remove(mRvCascadeFraction).remove(mRvChatFraction).remove(mRvStickyFraction)
                .remove(mLvFraction).remove(mGvFraction).submit();
    }

    @Override
    public void onRestoreAbilityState(PacMap inState) {
        super.onRestoreAbilityState(inState);
        position = inState.getIntValue("position");
        switch (position) {
            case 0:
                rvCascadeTab.select();
                break;
            case 1:
                rvStickyTab.select();
                break;
            case 2:
                rvChatTab.select();
                break;
            case 3:
                rvBingingTab.select();
                break;
            case 4:
                lvTab.select();
                break;
            case 5:
                gvTab.select();
                break;
            default:
                break;
        }
    }

    private void initManager() {
        mRvCascadeFraction = new RvCascadeFraction();
        mRvStickyFraction = new RvStickyFraction();
        mRvChatFraction = new RvChatFraction();
        mRvBindingFraction = new RvBindingFraction();
        mLvFraction = new LvFraction();
        mGvFraction = new GvFraction();
        mFractions.add(mRvCascadeFraction);
        mFractions.add(mRvStickyFraction);
        mFractions.add(mRvChatFraction);
        mFractions.add(mRvBindingFraction);
        mFractions.add(mLvFraction);
        mFractions.add(mGvFraction);

        mFractionManager = getFractionManager();
        createPageInContainer(ResourceTable.Id_stackLayout, 0);
    }


    private void initTab() {
        mTabList = (TabList) findComponentById(ResourceTable.Id_tabList);
        rvCascadeTab = mTabList.new Tab(this);
        rvCascadeTab.setText("RvCascade");
        mTabList.addTab(rvCascadeTab);
        rvStickyTab = mTabList.new Tab(this);
        rvStickyTab.setText("RvSticky");
        mTabList.addTab(rvStickyTab);
        rvChatTab = mTabList.new Tab(this);
        rvChatTab.setText("RvChat");
        mTabList.addTab(rvChatTab);
        rvBingingTab = mTabList.new Tab(this);
        rvBingingTab.setText("RvBinding");
        mTabList.addTab(rvBingingTab);
        lvTab = mTabList.new Tab(this);
        lvTab.setText("Lv");
        mTabList.addTab(lvTab);
        gvTab = mTabList.new Tab(this);
        gvTab.setText("Gv");
        mTabList.addTab(gvTab);
        rvCascadeTab.select();
        mTabList.addTabSelectedListener(new TabList.TabSelectedListener() {
            @Override
            public void onSelected(TabList.Tab tab) {
                position = tab.getPosition();
                createPageInContainer(ResourceTable.Id_stackLayout, position);
            }

            @Override
            public void onUnselected(TabList.Tab tab) {

            }

            @Override
            public void onReselected(TabList.Tab tab) {

            }
        });
    }

    private void createPageInContainer(int container, int position) {
        FractionScheduler fractionScheduler = mFractionManager.startFractionScheduler();
        if (mCurFraction != null) {
            fractionScheduler.hide(mCurFraction);
        }
        String tag = container + ":" + position;
        Fraction fraction;
        Optional<Fraction> fractionOptional = mFractionManager.getFractionByTag(tag);
        if (fractionOptional.isPresent()) {
            fraction = fractionOptional.get();
            fractionScheduler.show(fraction);
        } else {
            fraction = getPage(position);
            fractionScheduler.add(container, fraction, tag);
        }
        mCurFraction = fraction;
        fractionScheduler.submit();
    }

    private Fraction getPage(int position) {
        Optional<Fraction> fractionOptional = mFractionPlainArray.get(position);
        if (fractionOptional.isPresent()) {
            return fractionOptional.get();
        }
        Fraction fraction = mFractions.get(position);
        mFractionPlainArray.put(position, fraction);
        return fraction;
    }


}
