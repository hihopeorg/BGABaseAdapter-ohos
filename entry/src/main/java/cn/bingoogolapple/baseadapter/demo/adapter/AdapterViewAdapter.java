package cn.bingoogolapple.baseadapter.demo.adapter;

import cn.bingoogolapple.baseadapter.BGAAdapterViewAdapter;
import cn.bingoogolapple.baseadapter.BGAViewHolderHelper;
import cn.bingoogolapple.baseadapter.demo.ResourceTable;
import cn.bingoogolapple.baseadapter.demo.model.NormalModel;
import ohos.app.Context;

/**
 * 作者:王浩 邮件:bingoogolapple@gmail.com
 * 创建时间:15/5/21 上午12:39
 * 描述:简化 AdapterView 的子类（如 ListView、GridView）的适配器的编写
 */
public class AdapterViewAdapter extends BGAAdapterViewAdapter<NormalModel> {

    private int resId = 0;
    public AdapterViewAdapter(Context context) {
        super(context, ResourceTable.Layout_item_normal);
    }

    public AdapterViewAdapter(Context context, int res) {
        super(context, res);
        resId = res;
    }

    @Override
    protected void setItemChildListener(BGAViewHolderHelper helper) {
        helper.setItemChildClickListener(ResourceTable.Id_tv_item_normal_delete);
        helper.setItemChildLongClickListener(ResourceTable.Id_tv_item_normal_delete);
        helper.setItemChildCheckedChangeListener(ResourceTable.Id_cb_item_normal_status);
    }

    @Override
    public void fillData(BGAViewHolderHelper helper, int position, NormalModel model) {
        if (resId == 0) {
            helper.setText(ResourceTable.Id_tv_item_normal_title, model.title).setText(ResourceTable.Id_tv_item_normal_detail, model.detail);
        }
        helper.setChecked(ResourceTable.Id_cb_item_normal_status, model.selected);
        helper.setImageResource(ResourceTable.Id_iv_item_normal_avatar, model.res);
    }
}