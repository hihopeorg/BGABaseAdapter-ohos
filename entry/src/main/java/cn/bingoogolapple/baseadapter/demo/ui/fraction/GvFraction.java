package cn.bingoogolapple.baseadapter.demo.ui.fraction;

import cn.bingoogolapple.baseadapter.BGAOnItemChildCheckedChangeListener;
import cn.bingoogolapple.baseadapter.BGAOnItemChildClickListener;
import cn.bingoogolapple.baseadapter.BGAOnItemChildLongClickListener;
import cn.bingoogolapple.baseadapter.demo.ResourceTable;
import cn.bingoogolapple.baseadapter.demo.adapter.AdapterViewAdapter;
import cn.bingoogolapple.baseadapter.demo.engine.DataEngine;
import ohos.agp.components.*;
import ohos.agp.window.dialog.ToastDialog;

public class GvFraction extends MvcFraction implements ListContainer.ItemClickedListener, ListContainer.ItemLongClickedListener, BGAOnItemChildClickListener, BGAOnItemChildLongClickListener, BGAOnItemChildCheckedChangeListener {

    private ListContainer mDataLv;
    private AdapterViewAdapter mAdapter;

    @Override
    protected int getRootLayoutResID() {
        return ResourceTable.Layout_fragment_listview;
    }

    @Override
    protected void initView() {
        mDataLv = getViewById(ResourceTable.Id_lv_listview_data);
        TableLayoutManager layoutManager = new TableLayoutManager();
        layoutManager.setColumnCount(2);
        mDataLv.setLayoutManager(layoutManager);
    }

    @Override
    protected void setListener() {
        mDataLv.setItemClickedListener(this);
        mDataLv.setItemLongClickedListener(this);

        mAdapter = new AdapterViewAdapter(mAbility, ResourceTable.Layout_gv_item_normal);
        mAdapter.setOnItemChildClickListener(this);
        mAdapter.setOnItemChildLongClickListener(this);
        mAdapter.setOnItemChildCheckedChangeListener(this);
    }

    @Override
    protected void processLogic() {
        mAdapter.setData(DataEngine.loadData());
        mDataLv.setLongClickable(false);
        mDataLv.setItemProvider(mAdapter);
    }

    @Override
    public void onItemClicked(ListContainer listContainer, Component component, int position, long l) {
        new ToastDialog(this).setText("点击了条目 " + position).setDuration(1000).show();
    }

    @Override
    public boolean onItemLongClicked(ListContainer listContainer, Component component, int position, long l) {
        new ToastDialog(this).setText("长按了 " + position).setDuration(1000).show();
        return true;
    }

    @Override
    public void onItemChildClick(ComponentContainer parent, Component childView, int position) {
        if (childView.getId() == ResourceTable.Id_tv_item_normal_delete) {
            mAdapter.removeItem(position);
        }
    }

    @Override
    public boolean onItemChildLongClick(ComponentContainer parent, Component childView, int position) {
        if (childView.getId() == ResourceTable.Id_tv_item_normal_delete) {
            new ToastDialog(this).setText("长按了删除 " + position).setDuration(1000).show();
            return true;
        }
        return false;
    }

    @Override
    public void onItemChildCheckedChanged(ComponentContainer parent, Button childView, int position, boolean isChecked) {
        if (mAdapter.getItem(position).getSelected() == isChecked) {
            return;
        }
        mAdapter.getItem(position).setSelected(isChecked);
    }
}
