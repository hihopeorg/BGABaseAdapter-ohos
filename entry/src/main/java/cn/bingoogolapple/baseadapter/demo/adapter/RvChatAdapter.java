package cn.bingoogolapple.baseadapter.demo.adapter;

import cn.bingoogolapple.baseadapter.demo.ResourceTable;
import cn.bingoogolapple.baseadapter.demo.util.NinePatchHelper;
import ohos.agp.components.Component;

import cn.bingoogolapple.baseadapter.BGARecyclerViewAdapter;
import cn.bingoogolapple.baseadapter.BGAViewHolderHelper;
import cn.bingoogolapple.baseadapter.demo.model.ChatModel;
import ohos.agp.components.ListContainer;
import ohos.agp.text.RichText;
import ohos.agp.text.RichTextBuilder;
import ohos.agp.text.TextForm;
import ohos.agp.utils.Color;

/**
 * 作者:王浩 邮件:bingoogolapple@gmail.com
 * 创建时间:15/5/22 16:31
 * 描述:RecyclerView 多 item 类型适配器
 */
public class RvChatAdapter extends BGARecyclerViewAdapter<ChatModel> {

    public RvChatAdapter(ListContainer recyclerView) {
        super(recyclerView);
    }

    @Override
    public void setItemChildListener(BGAViewHolderHelper helper, int viewType) {
        if (viewType == ResourceTable.Layout_item_chat_me) {
            helper.setItemChildClickListener(ResourceTable.Id_iv_item_chat_me_status);
        }
    }

    @Override
    public int getItemComponentType(int position) {
        if (getItem(position).mUserType == ChatModel.UserType.Other) {
            return ResourceTable.Layout_item_chat_other;
        } else {
            return ResourceTable.Layout_item_chat_me;
        }
    }

    @Override
    public void fillData(BGAViewHolderHelper helper, int position, ChatModel model) {
        if (model.mUserType == ChatModel.UserType.Other) {
            RichTextBuilder builder = new RichTextBuilder();
            builder.mergeForm(new TextForm().setTextColor(Color.getIntColor("#FD51EB")).setTextSize(50));
            builder.addText("她发的");
            builder.revertForm();
            builder.mergeForm(new TextForm().setTextColor(Color.getIntColor("#EF8EB2")).setTextSize(50));
            builder.addText("彩色消息");
            builder.revertForm();
            builder.mergeForm(new TextForm().setTextColor(Color.getIntColor("#9674FC")).setTextSize(50));
            builder.addText("消息" + position);
            builder.revertForm();
            RichText richText = builder.build();
            helper.setRichText(ResourceTable.Id_tv_item_chat_other_msg, richText);
            NinePatchHelper.setNinePatchDrawTask(ResourceTable.Media_chat_from_normal, helper.getView(ResourceTable.Id_tv_item_chat_other_msg));
        } else {
            RichTextBuilder builder = new RichTextBuilder();
            builder.mergeForm(new TextForm().setTextColor(Color.getIntColor("#B3EB7D")).setTextSize(50));
            builder.addText("我发的");
            builder.revertForm();
            builder.mergeForm(new TextForm().setTextColor(Color.getIntColor("#8E92DB")).setTextSize(50));
            builder.addText("彩色消息");
            builder.revertForm();
            builder.mergeForm(new TextForm().setTextColor(Color.getIntColor("#38D369")).setTextSize(50));
            builder.addText("消息" + position);
            builder.revertForm();
            RichText richText = builder.build();
            helper.setRichText(ResourceTable.Id_tv_item_chat_me_msg, richText);
            NinePatchHelper.setNinePatchDrawTask(ResourceTable.Media_chat_to_normal, helper.getView(ResourceTable.Id_tv_item_chat_me_msg));
            helper.setVisibility(ResourceTable.Id_iv_item_chat_me_status, model.mSendStatus == ChatModel.SendStatus.Failure ? Component.VISIBLE : Component.HIDE);
        }
    }

}