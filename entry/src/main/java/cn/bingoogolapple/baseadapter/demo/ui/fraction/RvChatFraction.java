package cn.bingoogolapple.baseadapter.demo.ui.fraction;

import cn.bingoogolapple.baseadapter.BGAOnItemChildClickListener;
import cn.bingoogolapple.baseadapter.demo.ResourceTable;
import cn.bingoogolapple.baseadapter.demo.adapter.RvChatAdapter;
import cn.bingoogolapple.baseadapter.demo.engine.DataEngine;
import cn.bingoogolapple.baseadapter.demo.model.ChatModel;
import ohos.agp.components.*;

public class RvChatFraction extends MvcFraction implements BGAOnItemChildClickListener  {

    private RvChatAdapter mAdapter;
    private ListContainer mDataRv;

    @Override
    protected int getRootLayoutResID() {
        return ResourceTable.Layout_fraction_rv_chat;
    }

    @Override
    protected void initView() {
        mDataRv = getViewById(ResourceTable.Id_rv_data);
    }

    @Override
    protected void setListener() {
        mAdapter = new RvChatAdapter(mDataRv);
        mAdapter.setOnItemChildClickListener(this);
    }

    @Override
    protected void processLogic() {
        DirectionalLayoutManager layoutManager = new DirectionalLayoutManager();
        layoutManager.setOrientation(DirectionalLayout.VERTICAL);
        mDataRv.setLayoutManager(layoutManager);

        mAdapter.setData(DataEngine.loadChatModelData());
        mDataRv.setLongClickable(false);
        mDataRv.setItemProvider(mAdapter);
    }

    @Override
    public void onItemChildClick(ComponentContainer parent, Component childView, int position) {
        //mAdapter.getItem(position).mSendStatus = ChatModel.SendStatus.Success;

        //mAdapter.moveItem(position, mAdapter.getCount() - 1);
    }
}
